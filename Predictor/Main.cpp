#include "Predictor.h"

int main()
{
	//////////////////////////////////////////////////////////////////////////////////
	// ============================== Initialization ============================== //
	//////////////////////////////////////////////////////////////////////////////////

	Mat frame, frameGray, focusGray, focusBin;
	Rect boundRect;
	float minimum = 1000, maxFocus = 20;
	int thr = 0, zoneSize = 0, zoneSizeInit = 0;
	double sharpness = 0, sharpInit = 0;
	char charCheckForEscKey = 0;

	// Open a video-file
	string videoName = "1.mp4";
	VideoCapture capture(videoName);
	if (!capture.isOpened())
		throw "mp4 video-file is not found!";

	// Read until video-file ends
	while (1)
	{
		// ======================== Step 1. Frame reading ======================== //

		// Read the frame
		capture >> frame;
		if (frame.empty())
			break;

		// Rotate the frame
		rotate(frame, frame, ROTATE_90_CLOCKWISE);

		// ======================== Step 2. Frame processing ======================== //

		// 1. Convert the frame from BGR to grayscale
		cvtColor(frame, frameGray, CV_BGR2GRAY);

		// 2. Detect and extract the object blob (focusGray)
		focusGray = detectObj(frameGray, zoneSize);

		// 3. Otsu's thresholding
		threshold(focusGray, focusBin, 0, 255, CV_THRESH_BINARY_INV | THRESH_OTSU);

		// 4. Calculate sharpness
		sharpness = getSharpness(focusBin);

		// 5. Find minimal shapness
		if (sharpness < minimum)
		{
			minimum = sharpness;
			// minFocus = focus;   // focus distance 
								   // (Android feature),     output parameter 1
			sharpInit = minimum;						  // output parameter 2
			zoneSizeInit = zoneSize;					  // output parameter 3
		}

	}
	// ======================== Step 3. Display the results, close all frames ======================== //
	cout << "Shapness - " << sharpInit << endl;
	cout << "Zone size - " << zoneSizeInit << endl;
	float focusInit = 9.2;
	bool focusOk = ((focusInit > 0.2 * maxFocus) && (focusInit < 0.7 * maxFocus)) ? true : false;

	capture.release();
	destroyAllWindows();

	////////////////////////////////////////////////////////////////////////////////////
	// ================================= Prediction ================================= //
	////////////////////////////////////////////////////////////////////////////////////

	frameGray.release();
	focusGray.release();
	focusBin.release();
	zoneSize = 0;

	// Initialize the input parameters
	int zoneSizePrev = zoneSizeInit;
	float focusPred = focusInit,
		diff = 0;

	capture = VideoCapture(videoName);
	
	// Read until video-file ends
	while (1)
	{
		// ======================== Step 1. Frame reading ======================== //
		// Read the frame
		capture >> frame;
		if (frame.empty())
			break;

		// Rotate the frame
		rotate(frame, frame, ROTATE_90_CLOCKWISE);

		// ======================== Step 2. Frame processing ======================== //

		// 1. Convert the frame from BGR to grayscale
		cvtColor(frame, frameGray, CV_BGR2GRAY);

		// 2. Detect and extract the object blob (focusGray)
		focusGray = detectObj(frameGray, zoneSize);

		// 3. Predict focus value
		diff = 1.0 * abs(zoneSize - zoneSizePrev) / zoneSizePrev;
		if (diff > 0.05)
		{
			zoneSizePrev = zoneSize;
			predict(zoneSize, zoneSizeInit, focusInit, maxFocus, focusPred, focusOk); // focusPred, focusOk - output parameters
		}

		// If "write a log-file" do these steps

		// 4. Otsu's thresholding
		threshold(focusGray, focusBin, 0, 255, CV_THRESH_BINARY_INV | THRESH_OTSU);

		// 5. Calculate sharpness
		sharpness = getSharpness(focusBin);

		// 6. Write timestamps, lens position, zone size and sharpness into a txt-file
	}
	// ======================== Step 3. Close all frames ======================== //
	capture.release();
	destroyAllWindows();
	return 0;
}