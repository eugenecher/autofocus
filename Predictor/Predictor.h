#pragma once
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <chrono>
#include <vector>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

// Initialization
Mat detectObj(Mat frameGray, int &zoneSize);
double getSharpness(Mat focusBin);

// Prediction
//int getZoneSize(Mat frameGray);
void predict(int zoneSize, int zoneSizeInit, float focusInit, float maxFocus, float &focusPred, bool &focusOk);