#include "Logger.h"

int main()
{
	////////////////////////////////////////////////////////////////
	// ===================== Initialization ===================== //
	////////////////////////////////////////////////////////////////

	Mat frame, frameGray, focusGray, focusBin;
	Rect boundRect;
	int thr, textSize = 0;
	double sharpness = 0;
	string timeStamp, lensPos, restPars;
	char charCheckForEscKey = 0;

	string videoName = "19cm.mp4",
		intTxtName = "19cm.txt",
		outTxtName = "19cm_out.txt";

	// Create a window
	namedWindow("Frames", 1);

	// Open a video-file
	VideoCapture capture(videoName);
	if (!capture.isOpened())
		throw "Error when reading the video";

	// Open the input file and create an output one
	ifstream inFile(intTxtName);
	ofstream outFile(outTxtName);
	if (!inFile.is_open())
		throw "Input txt-filea is not available";

	/////////////////////////////////////////////////////////////////////////////////
	// ====================== Frames reading and processing ====================== //
	/////////////////////////////////////////////////////////////////////////////////

	// Start frames reading
	while (1)
	{
		// ======================== Step 1. Frame reading ======================== //

		// Read the frame
		capture >> frame;
		if (frame.empty())
			break;

		// Rotate the frame
		rotate(frame, frame, ROTATE_90_CLOCKWISE);		

		// ======================== Step 2. Frame processing ======================== //

		// Convert the color frame to grayscale
		cvtColor(frame, frameGray, CV_BGR2GRAY);

		//timeStart = high_resolution_clock::now();

		// Detect and extract the text (focus) region
		detectObj(frameGray, focusGray, boundRect, textSize);  // textSize - output parameter 1
		
		/*timEnd = high_resolution_clock::now();
		timePassed = duration_cast<microseconds>(timEnd - timeStart).count();*/

		// Otsu's thresholding
		thr = threshold(focusGray, focusBin, 0, 255, CV_THRESH_BINARY_INV | THRESH_OTSU);

		// Apply distance transform for edge width measuring
		sharpness = getSharpness(focusBin);						// sharpness - output parameter2

		// ======================== Step 3. Writing the data into a txt ======================== //
		
		// Read a timestamp
		getline(inFile, timeStamp, ' ');

		// Read a lens position
		getline(inFile, lensPos, '\n');
		outFile << timeStamp << " " << lensPos << " " << textSize << " " << sharpness << endl;
		//getline(inFile, restPars, '\n');

		// ======================== Displaying the results ======================== //
		/*
		// Display the detected blobs
		rectangle(frame, boundRect, CV_RGB(255, 0, 0));
		
		// Display the original frame
		putText(frame, format("Lens position: %3.1f", stof(lensPos)), Point(10, 40), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 0));
		putText(frame, format("Text size: %d pixels", textSize), Point(10, 80), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 0));
		putText(frame, format("Sharpness: %3.1f", sharpness), Point(10, 120), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 0));
		putText(frame, format("Threshold: %d", thr), Point(10, 160), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 0));
		//putText(frame, format("Frame processing time: %d us", timePassed), Point(frame.cols - 240, 40), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 0));
		imshow("Frames", frame);

		// Display the binary frame
		if (focusBin.size() != Size(0, 0))
			imshow("Focus window (binary)", focusBin);

		// Display the histogram
		plotHistGray(focusGray, thr);

		// Wait to display the frame
		charCheckForEscKey = waitKey(1);*/
	}
	// Close the files
	inFile.close();
	outFile.close();

	return 0;
}