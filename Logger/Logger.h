#pragma once
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <chrono>
#include <vector>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

double getSharpness(Mat focusBin);
void detectObj(Mat frameGray, Mat &objBlob, Rect &boundRect, int &zoneSize);