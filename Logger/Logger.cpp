#include "Logger.h"

double getSharpness(Mat focusBin)
{
	// This function calculates sharpness value using distance transform procedure
	// INPUTS:
	// focusBin - binary image of the whole frame
	// OUTPUTS:
	// sharpness - sharpness value

	// Init
	int count  = 0;	// number of border pixels
	double sum = 0; // sum of distances from each pixel to the nearest border

	// If the text blob was not detected
	if (focusBin.size() == Size(0, 0))
		return 0;

	// Apply distance transform within the window around the focus point
	Mat dist;
	distanceTransform(focusBin, dist, CV_DIST_L2, 3); // calculates how far each pixel is from the nearest black pixel
													  // the object pixels are white (255)
													  // all background pixels are black (0) and their distance is 0

	// Get the sharpness
	for (int i = 0; i < dist.cols; i++)
		for (int j = 0; j < dist.rows; j++)
		{
			if (dist.at<float>(j, i) > 0)
			{
				sum += dist.at<float>(j, i) * dist.at<float>(j, i); // find the sum of all distances squared
				count++;											// count the pixels with non-zero distance
			}
		}
	return sum / count; // average distance squared among all pixels
}

void detectObj(Mat frameGray, Mat &objBlob, Rect &boundRect, int &zoneSize)
{
	// This function detects text blob on the frame and returns its image
	// INPUTS:
	// frameGray - grayscale image of the whole frame
	// OUTPUTS:
	// boundRect - rectangle around the object blob
	// objBlob   - grayscale image of the object blob
	// zoneSize  - size of the object blob (size of boundrect)

	// Initialization
	Rect appRect;
	vector<vector<Point>> contours;
	Mat frameBin, frameSobel, element;
	int max = 0;
	zoneSize = 0;

	// Sobel operator
	float arr[3] = { -1, 0, 1 };
	Mat kernel = Mat(1, 3, CV_32F, &arr);
	filter2D(frameGray, frameSobel, -1, kernel, Point(-1, -1), 0, BORDER_DEFAULT);
	normalize(frameSobel, frameSobel, 0, 255, NORM_MINMAX, -1, Mat());

	// Thresholding
	threshold(frameSobel, frameBin, 50, 255, CV_THRESH_BINARY);

	// Morphological dilation. Transform the edges into solid regions
	element = getStructuringElement(MORPH_RECT, Size(25, 3));
	dilate(frameBin, frameBin, element);

	// Extract all the blobs
	findContours(frameBin, contours, 0, 1);

	// Find the largest landscape-oriented blob
	for (int i = 0; i < contours.size(); i++)
		if (contours[i].size() > max)
		{
			max = contours[i].size();

			appRect = boundingRect(Mat(contours[i])); // Creates a rectangle around contour (blob)
			if (appRect.width > appRect.height)		  // Blob aspect ratio check
			{
				boundRect = appRect;
			}
		}
	
	// Crop the text blob
	Rect crop(boundRect.x, boundRect.y, boundRect.width, boundRect.height);
	objBlob = frameGray(crop);

	// Calculate text size
	zoneSize = boundRect.width + boundRect.height;
}